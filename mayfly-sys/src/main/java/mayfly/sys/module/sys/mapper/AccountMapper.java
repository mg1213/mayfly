package mayfly.sys.module.sys.mapper;

import mayfly.core.base.mapper.BaseMapper;
import mayfly.sys.module.sys.entity.AccountDO;

/**
 * 管理员Mapper
 *
 * @author hml
 * @date 2018/6/27 下午2:35
 */
public interface AccountMapper extends BaseMapper<Integer, AccountDO> {
}
